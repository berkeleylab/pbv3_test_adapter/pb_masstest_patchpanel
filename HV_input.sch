EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 9
Title "Powerboard Masstest Patch Panel"
Date "2021-07-26"
Rev "A"
Comp "LBNL"
Comment1 "Timon Heim"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1700 1650 0    50   ~ 0
CAEN A1536DN\nHV power supply\nInterlock connection
$Comp
L Connector_Generic:Conn_01x04 J21
U 1 1 610B29D0
P 1900 2300
AR Path="/610B1C11/610B29D0" Ref="J21"  Part="1" 
AR Path="/611F04D2/610B29D0" Ref="J22"  Part="1" 
F 0 "J22" H 1818 1875 50  0000 C CNN
F 1 "Conn_01x04" H 1818 1966 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 1900 2300 50  0001 C CNN
F 3 "~" H 1900 2300 50  0001 C CNN
	1    1900 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 2400 2500 2400
Wire Wire Line
	2100 2300 2500 2300
Wire Wire Line
	2100 2200 2500 2200
Wire Wire Line
	2100 2100 2500 2100
Text Label 2200 2400 0    50   ~ 0
PAS_P
Text Label 2200 2300 0    50   ~ 0
PAS_N
Text Label 2200 2200 0    50   ~ 0
SIG_P
Text Label 2200 2100 0    50   ~ 0
SIG_N
Text Notes 1300 2900 0    50   ~ 0
The enable procedure is completed in one of the following ways:\n− short the PASSIVE INTERLOCK (see also p. 7) pins (lower couple).\n− supply the SIGNAL INTERLOCK (see also p. 7) pins (higher couple) with a +5 V (3-4mA) differential signal.\nThe INTERLOCK LED (red) is turned off as one of the actions above is performed.
Text HLabel 2500 2300 2    50   Input ~ 0
PAS_N
Text HLabel 2500 2400 2    50   Input ~ 0
PAS_P
Text HLabel 2500 2200 2    50   Input ~ 0
SIG_P
Text HLabel 2500 2100 2    50   Input ~ 0
SIG_N
$EndSCHEMATC
