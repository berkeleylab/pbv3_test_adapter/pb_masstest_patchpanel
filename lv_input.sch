EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title "Powerboard Masstest Patch Panel"
Date "2021-07-26"
Rev "A"
Comp "LBNL"
Comment1 "Timon Heim"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x08 J?
U 1 1 6005F484
P 1900 4250
AR Path="/6005F484" Ref="J?"  Part="1" 
AR Path="/600573E1/6005F484" Ref="J24"  Part="1" 
AR Path="/611CD6C3/6005F484" Ref="J?"  Part="1" 
AR Path="/611CD87B/6005F484" Ref="J?"  Part="1" 
AR Path="/611CDC4A/6005F484" Ref="J?"  Part="1" 
AR Path="/611CEA1C/6005F484" Ref="J?"  Part="1" 
AR Path="/611D441D/6005F484" Ref="J?"  Part="1" 
AR Path="/611D5A26/6005F484" Ref="J?"  Part="1" 
AR Path="/611DE30B/6005F484" Ref="J?"  Part="1" 
AR Path="/611DF1A3/6005F484" Ref="J?"  Part="1" 
AR Path="/612A5468/6005F484" Ref="J27"  Part="1" 
AR Path="/612AA7FD/6005F484" Ref="J30"  Part="1" 
F 0 "J30" H 1822 4765 50  0000 C CNN
F 1 "Conn_01x08_Shielded" H 1822 4674 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00062_1x08_P5.00mm_45Degree" H 1900 4250 50  0001 C CNN
F 3 "~" H 1900 4250 50  0001 C CNN
	1    1900 4250
	-1   0    0    -1  
$EndComp
$Comp
L Connector:DB26_Female_HighDensity_MountingHoles J?
U 1 1 6005F48A
P 5950 3100
AR Path="/6005F48A" Ref="J?"  Part="1" 
AR Path="/600573E1/6005F48A" Ref="J25"  Part="1" 
AR Path="/611CD6C3/6005F48A" Ref="J?"  Part="1" 
AR Path="/611CD87B/6005F48A" Ref="J?"  Part="1" 
AR Path="/611CDC4A/6005F48A" Ref="J?"  Part="1" 
AR Path="/611CEA1C/6005F48A" Ref="J?"  Part="1" 
AR Path="/611D441D/6005F48A" Ref="J?"  Part="1" 
AR Path="/611D5A26/6005F48A" Ref="J?"  Part="1" 
AR Path="/611DE30B/6005F48A" Ref="J?"  Part="1" 
AR Path="/611DF1A3/6005F48A" Ref="J?"  Part="1" 
AR Path="/612A5468/6005F48A" Ref="J28"  Part="1" 
AR Path="/612AA7FD/6005F48A" Ref="J31"  Part="1" 
F 0 "J31" H 5950 3967 50  0000 C CNN
F 1 "DB26_Female_HighDensity_MountingHoles" H 5950 3876 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-26-HD_Female_Vertical_P2.29x1.98mm_MountingHoles" H 5000 3500 50  0001 C CNN
F 3 " ~" H 5000 3500 50  0001 C CNN
	1    5950 3100
	1    0    0    -1  
$EndComp
Text Label 2150 2550 0    50   ~ 0
LV_P0
Wire Wire Line
	2100 4550 2600 4550
Wire Wire Line
	2100 4350 2600 4350
Wire Wire Line
	2100 4150 2600 4150
Wire Wire Line
	2100 3950 2600 3950
$Comp
L Connector_Generic:Conn_01x08 J?
U 1 1 6005F47E
P 1900 2850
AR Path="/6005F47E" Ref="J?"  Part="1" 
AR Path="/600573E1/6005F47E" Ref="J23"  Part="1" 
AR Path="/611CD6C3/6005F47E" Ref="J?"  Part="1" 
AR Path="/611CD87B/6005F47E" Ref="J?"  Part="1" 
AR Path="/611CDC4A/6005F47E" Ref="J?"  Part="1" 
AR Path="/611CEA1C/6005F47E" Ref="J?"  Part="1" 
AR Path="/611D441D/6005F47E" Ref="J?"  Part="1" 
AR Path="/611D5A26/6005F47E" Ref="J?"  Part="1" 
AR Path="/611DE30B/6005F47E" Ref="J?"  Part="1" 
AR Path="/611DF1A3/6005F47E" Ref="J?"  Part="1" 
AR Path="/612A5468/6005F47E" Ref="J26"  Part="1" 
AR Path="/612AA7FD/6005F47E" Ref="J29"  Part="1" 
F 0 "J29" H 1822 3365 50  0000 C CNN
F 1 "Conn_01x08_Shielded" H 1822 3274 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00062_1x08_P5.00mm_45Degree" H 1900 2850 50  0001 C CNN
F 3 "~" H 1900 2850 50  0001 C CNN
	1    1900 2850
	-1   0    0    -1  
$EndComp
Text Label 2150 2650 0    50   ~ 0
LV_N0
Text Label 2150 2750 0    50   ~ 0
LV_P1
Text Label 2150 2850 0    50   ~ 0
LV_N1
Text Label 2150 2950 0    50   ~ 0
LV_P2
Text Label 2150 3050 0    50   ~ 0
LV_N2
Text Label 2150 3150 0    50   ~ 0
LV_P3
Text Label 2150 3250 0    50   ~ 0
LV_N3
Wire Wire Line
	2100 4050 2600 4050
Wire Wire Line
	2100 4250 2600 4250
Wire Wire Line
	2100 4450 2600 4450
Wire Wire Line
	2100 4650 2600 4650
Text Label 2150 3950 0    50   ~ 0
LV_P4
Text Label 2150 4050 0    50   ~ 0
LV_N4
Text Label 2150 4150 0    50   ~ 0
LV_P5
Text Label 2150 4250 0    50   ~ 0
LV_N5
Text Label 2150 4350 0    50   ~ 0
LV_P6
Text Label 2150 4450 0    50   ~ 0
LV_N6
Text Label 2150 4550 0    50   ~ 0
LV_P7
Text Label 2150 4650 0    50   ~ 0
LV_N7
Wire Wire Line
	5650 2600 4650 2600
Wire Wire Line
	5650 2700 5450 2700
Wire Wire Line
	5650 2800 5050 2800
Wire Wire Line
	5650 2900 5050 2900
Wire Wire Line
	5650 3000 5050 3000
Wire Wire Line
	5650 3100 5050 3100
Wire Wire Line
	5650 3200 5050 3200
Wire Wire Line
	5650 3300 5050 3300
Wire Wire Line
	5650 3400 5050 3400
Wire Wire Line
	5650 3500 5050 3500
Wire Wire Line
	5650 3600 5050 3600
Wire Wire Line
	5650 3700 5050 3700
Wire Wire Line
	5650 3800 5050 3800
Wire Wire Line
	5650 3900 5050 3900
Wire Wire Line
	5650 4000 5050 4000
Wire Wire Line
	5650 4100 5050 4100
Wire Wire Line
	5650 4200 5050 4200
Wire Wire Line
	5650 4300 5050 4300
Wire Wire Line
	6250 4100 6950 4100
Wire Wire Line
	6250 3900 6800 3900
Wire Wire Line
	6250 3700 6700 3700
Wire Wire Line
	6250 3300 6950 3300
Wire Wire Line
	6250 3100 6800 3100
Wire Wire Line
	6250 2900 6700 2900
Text Label 5150 4300 0    50   ~ 0
S_N3
Text Label 5150 4200 0    50   ~ 0
S_N1
Text Label 5150 4100 0    50   ~ 0
S_P3
Text Label 5150 4000 0    50   ~ 0
S_P1
Text Label 5150 3900 0    50   ~ 0
S_N2
Text Label 5150 3800 0    50   ~ 0
S_N0
Text Label 5150 3700 0    50   ~ 0
S_P2
Text Label 5150 3600 0    50   ~ 0
S_P0
Text Label 5150 3500 0    50   ~ 0
S_N5
Text Label 5150 3400 0    50   ~ 0
S_N7
Text Label 5150 3300 0    50   ~ 0
S_P5
Text Label 5150 3200 0    50   ~ 0
S_P7
Text Label 5150 3100 0    50   ~ 0
S_N4
Text Label 5150 3000 0    50   ~ 0
S_N6
Text Label 5150 2900 0    50   ~ 0
S_P4
Text Label 5150 2800 0    50   ~ 0
S_P6
Text Label 5150 2700 0    50   ~ 0
VAUX
Text Label 5150 2600 0    50   ~ 0
DGND
Text Label 6400 2700 0    50   ~ 0
IT47_P
Text Label 6400 2900 0    50   ~ 0
IT47_N
Text Label 6400 3100 0    50   ~ 0
ST47_N
Text Label 6400 3300 0    50   ~ 0
ST47_P
Text Label 6400 3500 0    50   ~ 0
IT03_P
Text Label 6400 3700 0    50   ~ 0
IT03_N
Text Label 6400 3900 0    50   ~ 0
ST03_N
Text Label 6400 4100 0    50   ~ 0
ST03_P
Text Notes 5100 5050 0    50   ~ 0
CAEN A251x Low Voltage Module connections
Text Notes 4600 2500 0    50   ~ 0
12V/100mA
Text HLabel 5050 2700 0    50   Input ~ 0
VAUX
Text Notes 4650 1750 0    50   ~ 0
Status Operation:\n− Contact closed between ST+ and ST- when \n    no FAIL is present on one Group channel (0..3 and 4..7)\n− Contact open between ST+ and ST- when FAIL \n    is present on one Group channel (0..3 and 4..7)
Text Notes 4650 1100 0    50   ~ 0
Interlock Operation:\nOne Group (channels 0..3 and 4..7) can be \nENABLED in one of the following ways:\n− Short circuit IT- with DGND and IT+ with VAUX pins \n    on service connector\n− Send signal 4÷6V, ca. 5mA current between IT- and IT+
Text HLabel 2600 3250 2    50   Input ~ 0
LV_N3
Text HLabel 2600 3050 2    50   Input ~ 0
LV_N2
Text HLabel 2600 2850 2    50   Input ~ 0
LV_N1
Text HLabel 2600 2650 2    50   Input ~ 0
LV_N0
Text HLabel 2600 3150 2    50   Input ~ 0
LV_P3
Text HLabel 2600 2950 2    50   Input ~ 0
LV_P2
Text HLabel 2600 2750 2    50   Input ~ 0
LV_P1
Text HLabel 2600 2550 2    50   Input ~ 0
LV_P0
Wire Wire Line
	2100 3150 2600 3150
Wire Wire Line
	2100 2950 2600 2950
Wire Wire Line
	2100 2750 2600 2750
Wire Wire Line
	2100 2550 2600 2550
Wire Wire Line
	2100 3250 2600 3250
Wire Wire Line
	2100 3050 2600 3050
Wire Wire Line
	2100 2850 2600 2850
Wire Wire Line
	2100 2650 2600 2650
Text HLabel 2600 4650 2    50   Input ~ 0
LV_N7
Text HLabel 2600 4450 2    50   Input ~ 0
LV_N6
Text HLabel 2600 4250 2    50   Input ~ 0
LV_N5
Text HLabel 2600 4050 2    50   Input ~ 0
LV_N4
Text HLabel 2600 4550 2    50   Input ~ 0
LV_P7
Text HLabel 2600 4350 2    50   Input ~ 0
LV_P6
Text HLabel 2600 4150 2    50   Input ~ 0
LV_P5
Text HLabel 2600 3950 2    50   Input ~ 0
LV_P4
Text HLabel 5050 2800 0    50   Input ~ 0
S_P6
Text HLabel 5050 2900 0    50   Input ~ 0
S_P4
Text HLabel 5050 3200 0    50   Input ~ 0
S_P7
Text HLabel 5050 3300 0    50   Input ~ 0
S_P5
Text HLabel 5050 3600 0    50   Input ~ 0
S_P0
Text HLabel 5050 3700 0    50   Input ~ 0
S_P2
Text HLabel 5050 4000 0    50   Input ~ 0
S_P1
Text HLabel 5050 4100 0    50   Input ~ 0
S_P3
Text HLabel 5050 4300 0    50   Input ~ 0
S_N3
Text HLabel 5050 4200 0    50   Input ~ 0
S_N1
Text HLabel 5050 3900 0    50   Input ~ 0
S_N2
Text HLabel 5050 3800 0    50   Input ~ 0
S_N0
Text HLabel 5050 3500 0    50   Input ~ 0
S_N5
Text HLabel 5050 3400 0    50   Input ~ 0
S_N7
Text HLabel 5050 3100 0    50   Input ~ 0
S_N4
Text HLabel 5050 3000 0    50   Input ~ 0
S_N6
$Comp
L power:GND #PWR03
U 1 1 61218C10
P 5950 4650
AR Path="/600573E1/61218C10" Ref="#PWR03"  Part="1" 
AR Path="/611DE30B/61218C10" Ref="#PWR?"  Part="1" 
AR Path="/611DF1A3/61218C10" Ref="#PWR?"  Part="1" 
AR Path="/612A5468/61218C10" Ref="#PWR07"  Part="1" 
AR Path="/612AA7FD/61218C10" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 5950 4400 50  0001 C CNN
F 1 "GND" H 5955 4477 50  0000 C CNN
F 2 "" H 5950 4650 50  0001 C CNN
F 3 "" H 5950 4650 50  0001 C CNN
	1    5950 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4650 5950 4600
Text GLabel 1500 5750 0    50   Input ~ 0
GND
$Comp
L power:GND #PWR02
U 1 1 6121B454
P 1650 5800
AR Path="/600573E1/6121B454" Ref="#PWR02"  Part="1" 
AR Path="/611DE30B/6121B454" Ref="#PWR?"  Part="1" 
AR Path="/611DF1A3/6121B454" Ref="#PWR?"  Part="1" 
AR Path="/612A5468/6121B454" Ref="#PWR06"  Part="1" 
AR Path="/612AA7FD/6121B454" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 1650 5550 50  0001 C CNN
F 1 "GND" H 1655 5627 50  0000 C CNN
F 2 "" H 1650 5800 50  0001 C CNN
F 3 "" H 1650 5800 50  0001 C CNN
	1    1650 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 5750 1650 5750
Wire Wire Line
	1650 5750 1650 5800
Wire Wire Line
	5950 4600 4650 4600
Wire Wire Line
	4650 4600 4650 2600
Connection ~ 5950 4600
Wire Wire Line
	6700 4600 5950 4600
Wire Wire Line
	5450 2700 5450 2100
Wire Wire Line
	5450 2100 6950 2100
Wire Wire Line
	6950 2100 6950 2850
Connection ~ 5450 2700
Wire Wire Line
	5450 2700 5050 2700
Connection ~ 6950 3300
Wire Wire Line
	6950 3300 6950 4100
Wire Wire Line
	6700 4600 6700 3700
Wire Wire Line
	6700 3700 6700 2900
Connection ~ 6700 3700
Wire Wire Line
	7500 3500 7500 3450
Wire Wire Line
	7500 2700 7500 2650
Text HLabel 6800 3100 2    50   Input ~ 0
ST47
Text HLabel 6800 3900 2    50   Input ~ 0
ST03
Wire Wire Line
	6250 3500 7500 3500
Wire Wire Line
	6250 2700 7500 2700
Wire Wire Line
	7800 3250 7850 3250
Text HLabel 8000 2450 2    50   Input ~ 0
~IT47
Text HLabel 8000 3250 2    50   Input ~ 0
~IT03
Wire Wire Line
	6950 2100 7500 2100
Wire Wire Line
	7500 2100 7500 2250
Connection ~ 6950 2100
Wire Wire Line
	6950 2850 7500 2850
Wire Wire Line
	7500 2850 7500 3050
Connection ~ 6950 2850
Wire Wire Line
	6950 2850 6950 3300
Text Label 7500 2100 0    50   ~ 0
VAUX
Text Label 7500 2900 0    50   ~ 0
VAUX
$Comp
L Transistor_FET:BS250 Q1
U 1 1 61270A8C
P 7600 2450
AR Path="/600573E1/61270A8C" Ref="Q1"  Part="1" 
AR Path="/612A5468/61270A8C" Ref="Q3"  Part="1" 
AR Path="/612AA7FD/61270A8C" Ref="Q5"  Part="1" 
F 0 "Q5" H 7805 2404 50  0000 L CNN
F 1 "BS250" H 7805 2495 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 7800 2375 50  0001 L CIN
F 3 "http://www.vishay.com/docs/70209/70209.pdf" H 7600 2450 50  0001 L CNN
	1    7600 2450
	-1   0    0    1   
$EndComp
$Comp
L Transistor_FET:BS250 Q2
U 1 1 61273D6B
P 7600 3250
AR Path="/600573E1/61273D6B" Ref="Q2"  Part="1" 
AR Path="/612A5468/61273D6B" Ref="Q4"  Part="1" 
AR Path="/612AA7FD/61273D6B" Ref="Q6"  Part="1" 
F 0 "Q6" H 7805 3204 50  0000 L CNN
F 1 "BS250" H 7805 3295 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 7800 3175 50  0001 L CIN
F 3 "http://www.vishay.com/docs/70209/70209.pdf" H 7600 3250 50  0001 L CNN
	1    7600 3250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 6127F4A0
P 7850 3500
AR Path="/600573E1/6127F4A0" Ref="R2"  Part="1" 
AR Path="/612A5468/6127F4A0" Ref="R4"  Part="1" 
AR Path="/612AA7FD/6127F4A0" Ref="R6"  Part="1" 
F 0 "R6" H 7920 3546 50  0000 L CNN
F 1 "4k7" H 7920 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7780 3500 50  0001 C CNN
F 3 "~" H 7850 3500 50  0001 C CNN
	1    7850 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6127E6BC
P 7850 2650
AR Path="/600573E1/6127E6BC" Ref="R1"  Part="1" 
AR Path="/612A5468/6127E6BC" Ref="R3"  Part="1" 
AR Path="/612AA7FD/6127E6BC" Ref="R5"  Part="1" 
F 0 "R5" H 7920 2696 50  0000 L CNN
F 1 "4k7" H 7920 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7780 2650 50  0001 C CNN
F 3 "~" H 7850 2650 50  0001 C CNN
	1    7850 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 61285DDE
P 7850 2850
AR Path="/600573E1/61285DDE" Ref="#PWR04"  Part="1" 
AR Path="/611DE30B/61285DDE" Ref="#PWR?"  Part="1" 
AR Path="/611DF1A3/61285DDE" Ref="#PWR?"  Part="1" 
AR Path="/612A5468/61285DDE" Ref="#PWR08"  Part="1" 
AR Path="/612AA7FD/61285DDE" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 7850 2600 50  0001 C CNN
F 1 "GND" H 7855 2677 50  0000 C CNN
F 2 "" H 7850 2850 50  0001 C CNN
F 3 "" H 7850 2850 50  0001 C CNN
	1    7850 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 61286899
P 7850 3700
AR Path="/600573E1/61286899" Ref="#PWR05"  Part="1" 
AR Path="/611DE30B/61286899" Ref="#PWR?"  Part="1" 
AR Path="/611DF1A3/61286899" Ref="#PWR?"  Part="1" 
AR Path="/612A5468/61286899" Ref="#PWR09"  Part="1" 
AR Path="/612AA7FD/61286899" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 7850 3450 50  0001 C CNN
F 1 "GND" H 7855 3527 50  0000 C CNN
F 2 "" H 7850 3700 50  0001 C CNN
F 3 "" H 7850 3700 50  0001 C CNN
	1    7850 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 3700 7850 3650
Wire Wire Line
	7850 3350 7850 3250
Connection ~ 7850 3250
Wire Wire Line
	7850 3250 8000 3250
Wire Wire Line
	7850 2850 7850 2800
Wire Wire Line
	7800 2450 7850 2450
Wire Wire Line
	7850 2500 7850 2450
Connection ~ 7850 2450
Wire Wire Line
	7850 2450 8000 2450
$EndSCHEMATC
