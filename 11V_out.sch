EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 9
Title "Powerboard Masstest Patch Panel"
Date "2021-07-26"
Rev "A"
Comp "LBNL"
Comment1 "Timon Heim"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2550 1450 0    50   ~ 0
SENSE_P0
Text Label 1650 1450 0    50   ~ 0
SENSE_N0
Text Label 1650 1250 0    50   ~ 0
VIN_N0
Text Label 2550 1250 0    50   ~ 0
VIN_P0
Wire Wire Line
	1850 1250 1550 1250
Connection ~ 1850 1250
Wire Wire Line
	1850 1350 1850 1250
Wire Wire Line
	1950 1350 1850 1350
Wire Wire Line
	2550 1250 2850 1250
Connection ~ 2550 1250
Wire Wire Line
	2550 1350 2550 1250
Wire Wire Line
	2450 1350 2550 1350
Wire Wire Line
	2450 1250 2550 1250
Wire Wire Line
	1950 1250 1850 1250
Wire Wire Line
	1950 1450 1300 1450
Wire Wire Line
	2450 1450 3050 1450
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J32
U 1 1 6005BD53
P 2150 1350
AR Path="/600B1B77/6005BD53" Ref="J32"  Part="1" 
AR Path="/611E07CD/6005BD53" Ref="J?"  Part="1" 
AR Path="/612B8FA8/6005BD53" Ref="J42"  Part="1" 
F 0 "J32" H 2200 1667 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 1576 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 1350 50  0001 C CNN
F 3 "~" H 2150 1350 50  0001 C CNN
	1    2150 1350
	1    0    0    -1  
$EndComp
Text Label 2550 2050 0    50   ~ 0
SENSE_P1
Text Label 1650 2050 0    50   ~ 0
SENSE_N1
Text Label 1650 1850 0    50   ~ 0
VIN_N1
Text Label 2550 1850 0    50   ~ 0
VIN_P1
Wire Wire Line
	1850 1850 1550 1850
Connection ~ 1850 1850
Wire Wire Line
	1850 1950 1850 1850
Wire Wire Line
	1950 1950 1850 1950
Wire Wire Line
	2550 1850 2850 1850
Connection ~ 2550 1850
Wire Wire Line
	2550 1950 2550 1850
Wire Wire Line
	2450 1950 2550 1950
Wire Wire Line
	2450 1850 2550 1850
Wire Wire Line
	1950 1850 1850 1850
Wire Wire Line
	1950 2050 1300 2050
Wire Wire Line
	2450 2050 3050 2050
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J33
U 1 1 611047E1
P 2150 1950
AR Path="/600B1B77/611047E1" Ref="J33"  Part="1" 
AR Path="/611E07CD/611047E1" Ref="J?"  Part="1" 
AR Path="/612B8FA8/611047E1" Ref="J43"  Part="1" 
F 0 "J33" H 2200 2267 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 2176 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 1950 50  0001 C CNN
F 3 "~" H 2150 1950 50  0001 C CNN
	1    2150 1950
	1    0    0    -1  
$EndComp
Text Label 2550 2650 0    50   ~ 0
SENSE_P2
Text Label 1650 2650 0    50   ~ 0
SENSE_N2
Text Label 1650 2450 0    50   ~ 0
VIN_N2
Text Label 2550 2450 0    50   ~ 0
VIN_P2
Wire Wire Line
	1850 2450 1550 2450
Connection ~ 1850 2450
Wire Wire Line
	1850 2550 1850 2450
Wire Wire Line
	1950 2550 1850 2550
Wire Wire Line
	2550 2450 2850 2450
Connection ~ 2550 2450
Wire Wire Line
	2550 2550 2550 2450
Wire Wire Line
	2450 2550 2550 2550
Wire Wire Line
	2450 2450 2550 2450
Wire Wire Line
	1950 2450 1850 2450
Wire Wire Line
	1950 2650 1300 2650
Wire Wire Line
	2450 2650 3050 2650
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J34
U 1 1 611064F9
P 2150 2550
AR Path="/600B1B77/611064F9" Ref="J34"  Part="1" 
AR Path="/611E07CD/611064F9" Ref="J?"  Part="1" 
AR Path="/612B8FA8/611064F9" Ref="J44"  Part="1" 
F 0 "J34" H 2200 2867 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 2776 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 2550 50  0001 C CNN
F 3 "~" H 2150 2550 50  0001 C CNN
	1    2150 2550
	1    0    0    -1  
$EndComp
Text Label 2550 3250 0    50   ~ 0
SENSE_P3
Text Label 1650 3250 0    50   ~ 0
SENSE_N3
Text Label 1650 3050 0    50   ~ 0
VIN_N3
Text Label 2550 3050 0    50   ~ 0
VIN_P3
Wire Wire Line
	1850 3050 1550 3050
Connection ~ 1850 3050
Wire Wire Line
	1850 3150 1850 3050
Wire Wire Line
	1950 3150 1850 3150
Wire Wire Line
	2550 3050 2850 3050
Connection ~ 2550 3050
Wire Wire Line
	2550 3150 2550 3050
Wire Wire Line
	2450 3150 2550 3150
Wire Wire Line
	2450 3050 2550 3050
Wire Wire Line
	1950 3050 1850 3050
Wire Wire Line
	1950 3250 1300 3250
Wire Wire Line
	2450 3250 3050 3250
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J35
U 1 1 61106513
P 2150 3150
AR Path="/600B1B77/61106513" Ref="J35"  Part="1" 
AR Path="/611E07CD/61106513" Ref="J?"  Part="1" 
AR Path="/612B8FA8/61106513" Ref="J45"  Part="1" 
F 0 "J35" H 2200 3467 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 3376 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 3150 50  0001 C CNN
F 3 "~" H 2150 3150 50  0001 C CNN
	1    2150 3150
	1    0    0    -1  
$EndComp
Text Label 2550 3850 0    50   ~ 0
SENSE_P4
Text Label 1650 3850 0    50   ~ 0
SENSE_N4
Text Label 1650 3650 0    50   ~ 0
VIN_N4
Text Label 2550 3650 0    50   ~ 0
VIN_P4
Wire Wire Line
	1850 3650 1550 3650
Connection ~ 1850 3650
Wire Wire Line
	1850 3750 1850 3650
Wire Wire Line
	1950 3750 1850 3750
Wire Wire Line
	2550 3650 2850 3650
Connection ~ 2550 3650
Wire Wire Line
	2550 3750 2550 3650
Wire Wire Line
	2450 3750 2550 3750
Wire Wire Line
	2450 3650 2550 3650
Wire Wire Line
	1950 3650 1850 3650
Wire Wire Line
	1950 3850 1300 3850
Wire Wire Line
	2450 3850 3050 3850
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J36
U 1 1 6110C5C1
P 2150 3750
AR Path="/600B1B77/6110C5C1" Ref="J36"  Part="1" 
AR Path="/611E07CD/6110C5C1" Ref="J?"  Part="1" 
AR Path="/612B8FA8/6110C5C1" Ref="J46"  Part="1" 
F 0 "J36" H 2200 4067 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 3976 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 3750 50  0001 C CNN
F 3 "~" H 2150 3750 50  0001 C CNN
	1    2150 3750
	1    0    0    -1  
$EndComp
Text Label 2550 4450 0    50   ~ 0
SENSE_P5
Text Label 1650 4450 0    50   ~ 0
SENSE_N5
Text Label 1650 4250 0    50   ~ 0
VIN_N5
Text Label 2550 4250 0    50   ~ 0
VIN_P5
Wire Wire Line
	1850 4250 1550 4250
Connection ~ 1850 4250
Wire Wire Line
	1850 4350 1850 4250
Wire Wire Line
	1950 4350 1850 4350
Wire Wire Line
	2550 4250 2850 4250
Connection ~ 2550 4250
Wire Wire Line
	2550 4350 2550 4250
Wire Wire Line
	2450 4350 2550 4350
Wire Wire Line
	2450 4250 2550 4250
Wire Wire Line
	1950 4250 1850 4250
Wire Wire Line
	1950 4450 1300 4450
Wire Wire Line
	2450 4450 3050 4450
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J37
U 1 1 6110C5DB
P 2150 4350
AR Path="/600B1B77/6110C5DB" Ref="J37"  Part="1" 
AR Path="/611E07CD/6110C5DB" Ref="J?"  Part="1" 
AR Path="/612B8FA8/6110C5DB" Ref="J47"  Part="1" 
F 0 "J37" H 2200 4667 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 4576 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 4350 50  0001 C CNN
F 3 "~" H 2150 4350 50  0001 C CNN
	1    2150 4350
	1    0    0    -1  
$EndComp
Text Label 2550 5050 0    50   ~ 0
SENSE_P6
Text Label 1650 5050 0    50   ~ 0
SENSE_N6
Text Label 1650 4850 0    50   ~ 0
VIN_N6
Text Label 2550 4850 0    50   ~ 0
VIN_P6
Wire Wire Line
	1850 4850 1550 4850
Connection ~ 1850 4850
Wire Wire Line
	1850 4950 1850 4850
Wire Wire Line
	1950 4950 1850 4950
Wire Wire Line
	2550 4850 2850 4850
Connection ~ 2550 4850
Wire Wire Line
	2550 4950 2550 4850
Wire Wire Line
	2450 4950 2550 4950
Wire Wire Line
	2450 4850 2550 4850
Wire Wire Line
	1950 4850 1850 4850
Wire Wire Line
	1950 5050 1300 5050
Wire Wire Line
	2450 5050 3050 5050
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J38
U 1 1 6110C5F5
P 2150 4950
AR Path="/600B1B77/6110C5F5" Ref="J38"  Part="1" 
AR Path="/611E07CD/6110C5F5" Ref="J?"  Part="1" 
AR Path="/612B8FA8/6110C5F5" Ref="J48"  Part="1" 
F 0 "J38" H 2200 5267 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 5176 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 4950 50  0001 C CNN
F 3 "~" H 2150 4950 50  0001 C CNN
	1    2150 4950
	1    0    0    -1  
$EndComp
Text Label 2550 5650 0    50   ~ 0
SENSE_P7
Text Label 1650 5650 0    50   ~ 0
SENSE_N7
Text Label 1650 5450 0    50   ~ 0
VIN_N7
Text Label 2550 5450 0    50   ~ 0
VIN_P7
Wire Wire Line
	1850 5450 1550 5450
Connection ~ 1850 5450
Wire Wire Line
	1850 5550 1850 5450
Wire Wire Line
	1950 5550 1850 5550
Wire Wire Line
	2550 5450 2850 5450
Connection ~ 2550 5450
Wire Wire Line
	2550 5550 2550 5450
Wire Wire Line
	2450 5550 2550 5550
Wire Wire Line
	2450 5450 2550 5450
Wire Wire Line
	1950 5450 1850 5450
Wire Wire Line
	1950 5650 1300 5650
Wire Wire Line
	2450 5650 3050 5650
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J39
U 1 1 6110C60F
P 2150 5550
AR Path="/600B1B77/6110C60F" Ref="J39"  Part="1" 
AR Path="/611E07CD/6110C60F" Ref="J?"  Part="1" 
AR Path="/612B8FA8/6110C60F" Ref="J49"  Part="1" 
F 0 "J39" H 2200 5867 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 5776 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 5550 50  0001 C CNN
F 3 "~" H 2150 5550 50  0001 C CNN
	1    2150 5550
	1    0    0    -1  
$EndComp
Text Label 2550 6250 0    50   ~ 0
SENSE_P8
Text Label 1650 6250 0    50   ~ 0
SENSE_N8
Text Label 1650 6050 0    50   ~ 0
VIN_N8
Text Label 2550 6050 0    50   ~ 0
VIN_P8
Wire Wire Line
	1850 6050 1550 6050
Connection ~ 1850 6050
Wire Wire Line
	1850 6150 1850 6050
Wire Wire Line
	1950 6150 1850 6150
Wire Wire Line
	2550 6050 2850 6050
Connection ~ 2550 6050
Wire Wire Line
	2550 6150 2550 6050
Wire Wire Line
	2450 6150 2550 6150
Wire Wire Line
	2450 6050 2550 6050
Wire Wire Line
	1950 6050 1850 6050
Wire Wire Line
	1950 6250 1300 6250
Wire Wire Line
	2450 6250 3050 6250
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J40
U 1 1 611189C7
P 2150 6150
AR Path="/600B1B77/611189C7" Ref="J40"  Part="1" 
AR Path="/611E07CD/611189C7" Ref="J?"  Part="1" 
AR Path="/612B8FA8/611189C7" Ref="J50"  Part="1" 
F 0 "J40" H 2200 6467 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 6376 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 6150 50  0001 C CNN
F 3 "~" H 2150 6150 50  0001 C CNN
	1    2150 6150
	1    0    0    -1  
$EndComp
Text Label 2550 6850 0    50   ~ 0
SENSE_P9
Text Label 1650 6850 0    50   ~ 0
SENSE_N9
Text Label 1650 6650 0    50   ~ 0
VIN_N9
Text Label 2550 6650 0    50   ~ 0
VIN_P9
Wire Wire Line
	1850 6650 1550 6650
Connection ~ 1850 6650
Wire Wire Line
	1850 6750 1850 6650
Wire Wire Line
	1950 6750 1850 6750
Wire Wire Line
	2550 6650 2850 6650
Connection ~ 2550 6650
Wire Wire Line
	2550 6750 2550 6650
Wire Wire Line
	2450 6750 2550 6750
Wire Wire Line
	2450 6650 2550 6650
Wire Wire Line
	1950 6650 1850 6650
Wire Wire Line
	1950 6850 1300 6850
Wire Wire Line
	2450 6850 3050 6850
$Comp
L Connector_Generic:Conn_02x03_Top_Bottom J41
U 1 1 611189E1
P 2150 6750
AR Path="/600B1B77/611189E1" Ref="J41"  Part="1" 
AR Path="/611E07CD/611189E1" Ref="J?"  Part="1" 
AR Path="/612B8FA8/611189E1" Ref="J51"  Part="1" 
F 0 "J41" H 2200 7067 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 2200 6976 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-06A_2x03_P4.20mm_Vertical" H 2150 6750 50  0001 C CNN
F 3 "~" H 2150 6750 50  0001 C CNN
	1    2150 6750
	1    0    0    -1  
$EndComp
Text HLabel 2850 1250 2    50   Input ~ 0
VIN_P0
Text HLabel 2850 1850 2    50   Input ~ 0
VIN_P1
Text HLabel 2850 2450 2    50   Input ~ 0
VIN_P2
Text HLabel 2850 3050 2    50   Input ~ 0
VIN_P3
Text HLabel 2850 3650 2    50   Input ~ 0
VIN_P4
Text HLabel 2850 4250 2    50   Input ~ 0
VIN_P5
Text HLabel 2850 4850 2    50   Input ~ 0
VIN_P6
Text HLabel 2850 5450 2    50   Input ~ 0
VIN_P7
Text HLabel 2850 6050 2    50   Input ~ 0
VIN_P8
Text HLabel 2850 6650 2    50   Input ~ 0
VIN_P9
Text HLabel 1550 6050 0    50   Input ~ 0
VIN_N8
Text HLabel 1550 6650 0    50   Input ~ 0
VIN_N9
Text HLabel 1550 5450 0    50   Input ~ 0
VIN_N7
Text HLabel 1550 4850 0    50   Input ~ 0
VIN_N6
Text HLabel 1550 4250 0    50   Input ~ 0
VIN_N5
Text HLabel 1550 3650 0    50   Input ~ 0
VIN_N4
Text HLabel 1550 3050 0    50   Input ~ 0
VIN_N3
Text HLabel 1550 2450 0    50   Input ~ 0
VIN_N2
Text HLabel 1550 1850 0    50   Input ~ 0
VIN_N1
Text HLabel 1550 1250 0    50   Input ~ 0
VIN_N0
Text HLabel 3050 1450 2    50   Input ~ 0
SENSE_P0
Text HLabel 3050 2050 2    50   Input ~ 0
SENSE_P1
Text HLabel 3050 2650 2    50   Input ~ 0
SENSE_P2
Text HLabel 3050 3250 2    50   Input ~ 0
SENSE_P3
Text HLabel 3050 3850 2    50   Input ~ 0
SENSE_P4
Text HLabel 3050 4450 2    50   Input ~ 0
SENSE_P5
Text HLabel 3050 5050 2    50   Input ~ 0
SENSE_P6
Text HLabel 3050 5650 2    50   Input ~ 0
SENSE_P7
Text HLabel 3050 6250 2    50   Input ~ 0
SENSE_P8
Text HLabel 3050 6850 2    50   Input ~ 0
SENSE_P9
Text HLabel 1300 1450 0    50   Input ~ 0
SENSE_N0
Text HLabel 1300 2050 0    50   Input ~ 0
SENSE_N1
Text HLabel 1300 2650 0    50   Input ~ 0
SENSE_N2
Text HLabel 1300 3250 0    50   Input ~ 0
SENSE_N3
Text HLabel 1300 3850 0    50   Input ~ 0
SENSE_N4
Text HLabel 1300 4450 0    50   Input ~ 0
SENSE_N5
Text HLabel 1300 5050 0    50   Input ~ 0
SENSE_N6
Text HLabel 1300 5650 0    50   Input ~ 0
SENSE_N7
Text HLabel 1300 6250 0    50   Input ~ 0
SENSE_N8
Text HLabel 1300 6850 0    50   Input ~ 0
SENSE_N9
$EndSCHEMATC
