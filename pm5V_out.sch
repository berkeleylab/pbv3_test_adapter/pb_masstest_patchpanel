EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 9
Title "Powerboard Masstest Patch Panel"
Date "2021-07-26"
Rev "A"
Comp "LBNL"
Comment1 "Timon Heim"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F2124
P 3000 1950
AR Path="/600B1B77/600F2124" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F2124" Ref="J1"  Part="1" 
F 0 "J1" H 3050 2167 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 3050 2076 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 3000 1950 50  0001 C CNN
F 3 "~" H 3000 1950 50  0001 C CNN
	1    3000 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F212A
P 3000 2350
AR Path="/600B1B77/600F212A" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F212A" Ref="J2"  Part="1" 
F 0 "J2" H 3050 2567 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 3050 2476 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 3000 2350 50  0001 C CNN
F 3 "~" H 3000 2350 50  0001 C CNN
	1    3000 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F2130
P 3000 2750
AR Path="/600B1B77/600F2130" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F2130" Ref="J3"  Part="1" 
F 0 "J3" H 3050 2967 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 3050 2876 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 3000 2750 50  0001 C CNN
F 3 "~" H 3000 2750 50  0001 C CNN
	1    3000 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F2136
P 3000 3150
AR Path="/600B1B77/600F2136" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F2136" Ref="J4"  Part="1" 
F 0 "J4" H 3050 3367 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 3050 3276 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 3000 3150 50  0001 C CNN
F 3 "~" H 3000 3150 50  0001 C CNN
	1    3000 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F213C
P 3000 3550
AR Path="/600B1B77/600F213C" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F213C" Ref="J5"  Part="1" 
F 0 "J5" H 3050 3767 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 3050 3676 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 3000 3550 50  0001 C CNN
F 3 "~" H 3000 3550 50  0001 C CNN
	1    3000 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F2142
P 4100 1950
AR Path="/600B1B77/600F2142" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F2142" Ref="J6"  Part="1" 
F 0 "J6" H 4150 2167 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 4150 2076 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 4100 1950 50  0001 C CNN
F 3 "~" H 4100 1950 50  0001 C CNN
	1    4100 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F2148
P 4100 2350
AR Path="/600B1B77/600F2148" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F2148" Ref="J7"  Part="1" 
F 0 "J7" H 4150 2567 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 4150 2476 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 4100 2350 50  0001 C CNN
F 3 "~" H 4100 2350 50  0001 C CNN
	1    4100 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F214E
P 4100 2750
AR Path="/600B1B77/600F214E" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F214E" Ref="J8"  Part="1" 
F 0 "J8" H 4150 2967 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 4150 2876 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 4100 2750 50  0001 C CNN
F 3 "~" H 4100 2750 50  0001 C CNN
	1    4100 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F2154
P 4100 3150
AR Path="/600B1B77/600F2154" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F2154" Ref="J9"  Part="1" 
F 0 "J9" H 4150 3367 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 4150 3276 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 4100 3150 50  0001 C CNN
F 3 "~" H 4100 3150 50  0001 C CNN
	1    4100 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 600F215A
P 4100 3550
AR Path="/600B1B77/600F215A" Ref="J?"  Part="1" 
AR Path="/600EAF16/600F215A" Ref="J10"  Part="1" 
F 0 "J10" H 4150 3767 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 4150 3676 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 4100 3550 50  0001 C CNN
F 3 "~" H 4100 3550 50  0001 C CNN
	1    4100 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2050 3400 2050
Wire Wire Line
	3300 1950 3350 1950
Wire Wire Line
	2800 1950 2500 1950
Wire Wire Line
	2500 1950 2500 2050
Wire Wire Line
	2800 2050 2500 2050
Connection ~ 2500 2050
Wire Wire Line
	2500 2050 2500 2350
Wire Wire Line
	2800 2350 2500 2350
Connection ~ 2500 2350
Wire Wire Line
	2500 2350 2500 2450
Wire Wire Line
	2800 2450 2500 2450
Connection ~ 2500 2450
Wire Wire Line
	2500 2450 2500 2750
Wire Wire Line
	2800 2750 2500 2750
Connection ~ 2500 2750
Wire Wire Line
	2500 2750 2500 2850
Wire Wire Line
	2800 2850 2500 2850
Connection ~ 2500 2850
Wire Wire Line
	2500 2850 2500 3150
Wire Wire Line
	2800 3150 2500 3150
Connection ~ 2500 3150
Wire Wire Line
	2500 3150 2500 3250
Wire Wire Line
	2800 3250 2500 3250
Connection ~ 2500 3250
Wire Wire Line
	2500 3250 2500 3550
Wire Wire Line
	2800 3550 2500 3550
Connection ~ 2500 3550
Wire Wire Line
	2500 3550 2500 3650
Wire Wire Line
	2800 3650 2500 3650
Wire Wire Line
	3300 2350 3350 2350
Wire Wire Line
	3350 2350 3350 1950
Wire Wire Line
	3300 2750 3350 2750
Wire Wire Line
	3350 2750 3350 2350
Connection ~ 3350 2350
Wire Wire Line
	3300 3150 3350 3150
Wire Wire Line
	3350 3150 3350 2750
Connection ~ 3350 2750
Wire Wire Line
	3300 3550 3350 3550
Wire Wire Line
	3350 3550 3350 3150
Connection ~ 3350 3150
Wire Wire Line
	3300 2450 3400 2450
Wire Wire Line
	3400 2450 3400 2050
Wire Wire Line
	3300 2850 3400 2850
Wire Wire Line
	3400 2850 3400 2450
Connection ~ 3400 2450
Wire Wire Line
	3300 3250 3400 3250
Wire Wire Line
	3400 3250 3400 2850
Connection ~ 3400 2850
Wire Wire Line
	3300 3650 3400 3650
Wire Wire Line
	3400 3650 3400 3250
Connection ~ 3400 3250
Wire Wire Line
	3900 1950 3650 1950
Wire Wire Line
	3650 1950 3650 2050
Wire Wire Line
	3900 2050 3650 2050
Connection ~ 3650 2050
Wire Wire Line
	3650 2050 3650 2350
Wire Wire Line
	3900 2350 3650 2350
Connection ~ 3650 2350
Wire Wire Line
	3650 2350 3650 2450
Wire Wire Line
	3900 2450 3650 2450
Connection ~ 3650 2450
Wire Wire Line
	3650 2450 3650 2750
Wire Wire Line
	3900 2750 3650 2750
Connection ~ 3650 2750
Wire Wire Line
	3650 2750 3650 2850
Wire Wire Line
	3900 2850 3650 2850
Connection ~ 3650 2850
Wire Wire Line
	3650 2850 3650 3150
Wire Wire Line
	3650 3150 3900 3150
Connection ~ 3650 3150
Wire Wire Line
	3650 3150 3650 3250
Wire Wire Line
	3900 3250 3650 3250
Connection ~ 3650 3250
Wire Wire Line
	3650 3250 3650 3550
Wire Wire Line
	3900 3550 3650 3550
Connection ~ 3650 3550
Wire Wire Line
	3650 3550 3650 3650
Wire Wire Line
	3900 3650 3650 3650
Wire Wire Line
	4400 1950 4500 1950
Wire Wire Line
	4400 2050 4550 2050
Wire Wire Line
	4400 2350 4500 2350
Wire Wire Line
	4500 2350 4500 1950
Wire Wire Line
	4400 2750 4500 2750
Connection ~ 4500 2350
Wire Wire Line
	4400 3150 4500 3150
Wire Wire Line
	4500 2350 4500 2750
Connection ~ 4500 2750
Wire Wire Line
	4500 2750 4500 3150
Wire Wire Line
	4400 3550 4500 3550
Wire Wire Line
	4500 3550 4500 3150
Connection ~ 4500 3150
Wire Wire Line
	4400 2450 4550 2450
Wire Wire Line
	4550 2450 4550 2050
Wire Wire Line
	4550 2450 4550 2850
Wire Wire Line
	4550 2850 4400 2850
Connection ~ 4550 2450
Wire Wire Line
	4400 3250 4550 3250
Connection ~ 4550 2850
Wire Wire Line
	4550 3650 4400 3650
Wire Wire Line
	4550 2850 4550 3250
Connection ~ 4550 3250
Wire Wire Line
	4550 3250 4550 3650
Wire Wire Line
	3350 1950 3350 1150
Wire Wire Line
	3350 1150 4500 1150
Connection ~ 3350 1950
Wire Wire Line
	3400 2050 3400 950 
Wire Wire Line
	3400 950  4550 950 
Connection ~ 3400 2050
Wire Wire Line
	2500 1950 2500 1050
Wire Wire Line
	2500 1050 3650 1050
Connection ~ 2500 1950
Wire Wire Line
	3650 1950 3650 1050
Connection ~ 3650 1950
Connection ~ 3650 1050
Wire Wire Line
	3650 1050 4700 1050
Wire Wire Line
	4500 1950 4500 1150
Connection ~ 4500 1950
Connection ~ 4500 1150
Wire Wire Line
	4500 1150 4700 1150
Wire Wire Line
	4550 2050 4550 950 
Connection ~ 4550 2050
Connection ~ 4550 950 
Wire Wire Line
	4550 950  4700 950 
Text Label 4050 1150 0    50   ~ 0
P5V_0
Text Label 4050 1050 0    50   ~ 0
GND_5V_0
Text Label 4050 950  0    50   ~ 0
N3V_0
Text HLabel 4700 1150 2    50   Input ~ 0
P5V_0
Text HLabel 4700 1050 2    50   Input ~ 0
GND_5V_0
Text HLabel 4700 950  2    50   Input ~ 0
N3V_0
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 619448FB
P 5800 1950
AR Path="/600B1B77/619448FB" Ref="J?"  Part="1" 
AR Path="/600EAF16/619448FB" Ref="J11"  Part="1" 
F 0 "J11" H 5850 2167 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 5850 2076 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 5800 1950 50  0001 C CNN
F 3 "~" H 5800 1950 50  0001 C CNN
	1    5800 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 61944905
P 5800 2350
AR Path="/600B1B77/61944905" Ref="J?"  Part="1" 
AR Path="/600EAF16/61944905" Ref="J12"  Part="1" 
F 0 "J12" H 5850 2567 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 5850 2476 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 5800 2350 50  0001 C CNN
F 3 "~" H 5800 2350 50  0001 C CNN
	1    5800 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 6194490F
P 5800 2750
AR Path="/600B1B77/6194490F" Ref="J?"  Part="1" 
AR Path="/600EAF16/6194490F" Ref="J13"  Part="1" 
F 0 "J13" H 5850 2967 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 5850 2876 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 5800 2750 50  0001 C CNN
F 3 "~" H 5800 2750 50  0001 C CNN
	1    5800 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 61944919
P 5800 3150
AR Path="/600B1B77/61944919" Ref="J?"  Part="1" 
AR Path="/600EAF16/61944919" Ref="J14"  Part="1" 
F 0 "J14" H 5850 3367 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 5850 3276 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 5800 3150 50  0001 C CNN
F 3 "~" H 5800 3150 50  0001 C CNN
	1    5800 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 61944923
P 5800 3550
AR Path="/600B1B77/61944923" Ref="J?"  Part="1" 
AR Path="/600EAF16/61944923" Ref="J15"  Part="1" 
F 0 "J15" H 5850 3767 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 5850 3676 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 5800 3550 50  0001 C CNN
F 3 "~" H 5800 3550 50  0001 C CNN
	1    5800 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 6194492D
P 6900 1950
AR Path="/600B1B77/6194492D" Ref="J?"  Part="1" 
AR Path="/600EAF16/6194492D" Ref="J16"  Part="1" 
F 0 "J16" H 6950 2167 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 6950 2076 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 6900 1950 50  0001 C CNN
F 3 "~" H 6900 1950 50  0001 C CNN
	1    6900 1950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 61944937
P 6900 2350
AR Path="/600B1B77/61944937" Ref="J?"  Part="1" 
AR Path="/600EAF16/61944937" Ref="J17"  Part="1" 
F 0 "J17" H 6950 2567 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 6950 2476 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 6900 2350 50  0001 C CNN
F 3 "~" H 6900 2350 50  0001 C CNN
	1    6900 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 61944941
P 6900 2750
AR Path="/600B1B77/61944941" Ref="J?"  Part="1" 
AR Path="/600EAF16/61944941" Ref="J18"  Part="1" 
F 0 "J18" H 6950 2967 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 6950 2876 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 6900 2750 50  0001 C CNN
F 3 "~" H 6900 2750 50  0001 C CNN
	1    6900 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 6194494B
P 6900 3150
AR Path="/600B1B77/6194494B" Ref="J?"  Part="1" 
AR Path="/600EAF16/6194494B" Ref="J19"  Part="1" 
F 0 "J19" H 6950 3367 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 6950 3276 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 6900 3150 50  0001 C CNN
F 3 "~" H 6900 3150 50  0001 C CNN
	1    6900 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Top_Bottom J?
U 1 1 61944955
P 6900 3550
AR Path="/600B1B77/61944955" Ref="J?"  Part="1" 
AR Path="/600EAF16/61944955" Ref="J20"  Part="1" 
F 0 "J20" H 6950 3767 50  0000 C CNN
F 1 "Conn_02x02_Top_Bottom" H 6950 3676 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-04A_2x02_P4.20mm_Vertical" H 6900 3550 50  0001 C CNN
F 3 "~" H 6900 3550 50  0001 C CNN
	1    6900 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2050 6200 2050
Wire Wire Line
	6100 1950 6150 1950
Wire Wire Line
	5600 1950 5300 1950
Wire Wire Line
	5300 1950 5300 2050
Wire Wire Line
	5600 2050 5300 2050
Connection ~ 5300 2050
Wire Wire Line
	5300 2050 5300 2350
Wire Wire Line
	5600 2350 5300 2350
Connection ~ 5300 2350
Wire Wire Line
	5300 2350 5300 2450
Wire Wire Line
	5600 2450 5300 2450
Connection ~ 5300 2450
Wire Wire Line
	5300 2450 5300 2750
Wire Wire Line
	5600 2750 5300 2750
Connection ~ 5300 2750
Wire Wire Line
	5300 2750 5300 2850
Wire Wire Line
	5600 2850 5300 2850
Connection ~ 5300 2850
Wire Wire Line
	5300 2850 5300 3150
Wire Wire Line
	5600 3150 5300 3150
Connection ~ 5300 3150
Wire Wire Line
	5300 3150 5300 3250
Wire Wire Line
	5600 3250 5300 3250
Connection ~ 5300 3250
Wire Wire Line
	5300 3250 5300 3550
Wire Wire Line
	5600 3550 5300 3550
Connection ~ 5300 3550
Wire Wire Line
	5300 3550 5300 3650
Wire Wire Line
	5600 3650 5300 3650
Wire Wire Line
	6100 2350 6150 2350
Wire Wire Line
	6150 2350 6150 1950
Wire Wire Line
	6100 2750 6150 2750
Wire Wire Line
	6150 2750 6150 2350
Connection ~ 6150 2350
Wire Wire Line
	6100 3150 6150 3150
Wire Wire Line
	6150 3150 6150 2750
Connection ~ 6150 2750
Wire Wire Line
	6100 3550 6150 3550
Wire Wire Line
	6150 3550 6150 3150
Connection ~ 6150 3150
Wire Wire Line
	6100 2450 6200 2450
Wire Wire Line
	6200 2450 6200 2050
Wire Wire Line
	6100 2850 6200 2850
Wire Wire Line
	6200 2850 6200 2450
Connection ~ 6200 2450
Wire Wire Line
	6100 3250 6200 3250
Wire Wire Line
	6200 3250 6200 2850
Connection ~ 6200 2850
Wire Wire Line
	6100 3650 6200 3650
Wire Wire Line
	6200 3650 6200 3250
Connection ~ 6200 3250
Wire Wire Line
	6700 1950 6450 1950
Wire Wire Line
	6450 1950 6450 2050
Wire Wire Line
	6700 2050 6450 2050
Connection ~ 6450 2050
Wire Wire Line
	6450 2050 6450 2350
Wire Wire Line
	6700 2350 6450 2350
Connection ~ 6450 2350
Wire Wire Line
	6450 2350 6450 2450
Wire Wire Line
	6700 2450 6450 2450
Connection ~ 6450 2450
Wire Wire Line
	6450 2450 6450 2750
Wire Wire Line
	6700 2750 6450 2750
Connection ~ 6450 2750
Wire Wire Line
	6450 2750 6450 2850
Wire Wire Line
	6700 2850 6450 2850
Connection ~ 6450 2850
Wire Wire Line
	6450 2850 6450 3150
Wire Wire Line
	6450 3150 6700 3150
Connection ~ 6450 3150
Wire Wire Line
	6450 3150 6450 3250
Wire Wire Line
	6700 3250 6450 3250
Connection ~ 6450 3250
Wire Wire Line
	6450 3250 6450 3550
Wire Wire Line
	6700 3550 6450 3550
Connection ~ 6450 3550
Wire Wire Line
	6450 3550 6450 3650
Wire Wire Line
	6700 3650 6450 3650
Wire Wire Line
	7200 1950 7300 1950
Wire Wire Line
	7200 2050 7350 2050
Wire Wire Line
	7200 2350 7300 2350
Wire Wire Line
	7300 2350 7300 1950
Wire Wire Line
	7200 2750 7300 2750
Connection ~ 7300 2350
Wire Wire Line
	7200 3150 7300 3150
Wire Wire Line
	7300 2350 7300 2750
Connection ~ 7300 2750
Wire Wire Line
	7300 2750 7300 3150
Wire Wire Line
	7200 3550 7300 3550
Wire Wire Line
	7300 3550 7300 3150
Connection ~ 7300 3150
Wire Wire Line
	7200 2450 7350 2450
Wire Wire Line
	7350 2450 7350 2050
Wire Wire Line
	7350 2450 7350 2850
Wire Wire Line
	7350 2850 7200 2850
Connection ~ 7350 2450
Wire Wire Line
	7200 3250 7350 3250
Connection ~ 7350 2850
Wire Wire Line
	7350 3650 7200 3650
Wire Wire Line
	7350 2850 7350 3250
Connection ~ 7350 3250
Wire Wire Line
	7350 3250 7350 3650
Wire Wire Line
	6150 1950 6150 1150
Wire Wire Line
	6150 1150 7300 1150
Connection ~ 6150 1950
Wire Wire Line
	6200 2050 6200 950 
Wire Wire Line
	6200 950  7350 950 
Connection ~ 6200 2050
Wire Wire Line
	5300 1950 5300 1050
Wire Wire Line
	5300 1050 6450 1050
Connection ~ 5300 1950
Wire Wire Line
	6450 1950 6450 1050
Connection ~ 6450 1950
Connection ~ 6450 1050
Wire Wire Line
	6450 1050 7500 1050
Wire Wire Line
	7300 1950 7300 1150
Connection ~ 7300 1950
Connection ~ 7300 1150
Wire Wire Line
	7300 1150 7500 1150
Wire Wire Line
	7350 2050 7350 950 
Connection ~ 7350 2050
Connection ~ 7350 950 
Wire Wire Line
	7350 950  7500 950 
Text Label 6850 1150 0    50   ~ 0
P5V_1
Text Label 6850 1050 0    50   ~ 0
GND_5V_1
Text Label 6850 950  0    50   ~ 0
N3V_1
Text HLabel 7500 1150 2    50   Input ~ 0
P5V_1
Text HLabel 7500 1050 2    50   Input ~ 0
GND_5V_1
Text HLabel 7500 950  2    50   Input ~ 0
N3V_1
$EndSCHEMATC
